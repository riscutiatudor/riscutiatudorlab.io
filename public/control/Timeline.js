sap.ui.define([
    "sap/ui/core/Control",
    "./TimelineRenderer"
], function (Control, TimelineRenderer) {
    "use strict";

    var oTimeline = Control.extend("ro.riscutiatudor.control.Timeline", {
        metadata: {
            properties: {
                width: {
                    type: "sap.ui.core.CSSSize",
                    defaultValue: "100%"
                },
                height: {
                    type: "sap.ui.core.CSSSize",
                    defaultValue: "100%"
                }
            },
            aggregations: {
                blocks: {
                    type: "ro.riscutiatudor.control.TimelineBlock",
                    multiple: true,
                    singularName: "block",
                    bindable: "bindable"
                }
            }
        },
        renderer: TimelineRenderer
    });

    return oTimeline;

});