sap.ui.define([],
	function() {
		"use strict";

		var oTimelineBlockRenderer = {};

		oTimelineBlockRenderer.render = function(oRm, oControl) {
			oRm.write("<div");
			oRm.writeControlData(oControl);
			oRm.addClass("container");
			oRm.addClass(oControl.getOrientation());
			oRm.writeClasses();
			oRm.write(">");
			oRm.write("<div");
			oRm.addClass("content");
			oRm.writeClasses();
			oRm.write(">");
			oRm.write("<h2");
			oRm.write(">");
			oRm.writeEscaped(oControl.getTitle());
			oRm.write("</h2>");
			oRm.renderControl(oControl._oContentSubtitle);
			oRm.renderControl(oControl._oContentText);			
			oRm.write("</div>");
			oRm.write("</div>");
		};

		return oTimelineBlockRenderer;

	}
);