sap.ui.define([
	"sap/ui/core/Control",
	"sap/m/Title",
	"sap/m/Text",
	"sap/m/Label",
	"./TimelineBlockRenderer"
], function(Control, Title, Text, Label, TimelineBlockRenderer) {
	"use strict";

	var oTimelineBlock = Control.extend("ro.riscutiatudor.control.TimelineBlock", {
		metadata: {
			properties: {
				width: {
					type: "sap.ui.core.CSSSize",
					defaultValue: "100%"
				},
				height: {
					type: "sap.ui.core.CSSSize",
					defaultValue: "100%"
				},
				orientation: {
					type: "string",
					defaultValue : null
				},
				title: {
					type: "string",
					defaultValue : null
				},
				subtitle: {
					type: "string",
					defaultValue : null
						
				},
				text: {
					type: "string",
					defaultValue : null
				}
			},
			aggregations: {
				_contentTitle: {
					type : "sap.m.Title",
					multiple : false,
					visibility : "hidden"
				},
				_contentSubtitle: {
					type : "sap.m.Label",
					multiple : false,
					visibility : "hidden"
				},				
				_contentText : {
					type : "sap.m.Text",
					multiple : false,
					visibility : "hidden"
				}
            }
		},
		
		renderer: TimelineBlockRenderer
	});

	/**
	* Init function for the control
	*/
	oTimelineBlock.prototype.init = function() {
		this._oContentTitle = new Title(this.getId() + "-content-title", {
			titleLevel : "H2"
		});
		this.setAggregation("_contentTitle", this._oContentTitle, true);
		
		this._oContentSubtitle = new Label(this.getId() + "-content-subtitle");
		this.setAggregation("_contentSubtitle", this._oContentSubtitle, true);
		
		this._oContentText = new Text(this.getId() + "-content-text");
		this._oContentText.cacheLineHeight = false;
		this.setAggregation("_contentText", this._oContentText, true);
	};

	oTimelineBlock.prototype.setTitle = function(sTitle) {
		this._oContentTitle.setText(sTitle);
		return this.setProperty("title", sTitle, true);
	};
	
	oTimelineBlock.prototype.setSubtitle = function(sSubtitle) {
		this._oContentSubtitle.setText(sSubtitle);
		return this.setProperty("subtitle", sSubtitle, true);
	};
	
	oTimelineBlock.prototype.setText = function(sText) {
		this._oContentText.setText(sText);
		return this.setProperty("text", sText, true);
	};
	
	return oTimelineBlock;

});