sap.ui.define([
	"sap/ui/core/mvc/Controller"
], function(Controller) {
	"use strict";

	/**
	 * Component
	 *
	 * @author Tudor Riscutia
	 * @version 1.0
	 */
	return Controller.extend("ro.riscutiatudor.controller.MyPage", {

		/* =========================================================== */
		/* event handlers */
		/* =========================================================== */

		/* Contact */
		
		/**
		 * Trigger emailto:riscutia.tudor@gmail.com
		 * 
		 * @public
		 */
		onEmail : function() {
			sap.m.URLHelper.triggerEmail("riscutia.tudor@gmail.com");
		},
		
		/**
		 * Trigger phone call to +49 1512 0115162
		 * 
		 * @public
		 */
		onPhone : function() {
			sap.m.URLHelper.triggerTel("+4915120115162");
		},
				
		/* Social media */
		
		/**
		 * Redirect to Twitter account
		 * 
		 * @public
		 */
		onTwitter : function() {
			sap.m.URLHelper.redirect("https://twitter.com/riscutiatudor", true);
		},

		/**
		 * Redirect to LinkedIn account
		 * 
		 * @public
		 */
		onLinkedIn : function() {
			sap.m.URLHelper.redirect("https://www.linkedin.com/in/tudor-riscutia-89293266/", true);
		},

		/**
		 * Redirect to XING account
		 * 
		 * @public
		 */
		onXing : function() {
			sap.m.URLHelper.redirect("https://www.xing.com/profile/Tudor_Riscutia", true);
		},
		
		/* Blog */
		
		/**
		 * Redirect to SAP Community blog
		 * 
		 * @public
		 */
		onBlog : function() {
			sap.m.URLHelper.redirect("https://people.sap.com/tudor.riscutia", true);			
		},
		
		onBlogsitTSR : function() {
			sap.m.URLHelper.redirect("https://blogs.sap.com/2018/04/17/sap-inside-track-timisoara-2018-sittsr/", true);					
		},
		
		onBlogRoadToSCP : function() {
			sap.m.URLHelper.redirect("https://blogs.sap.com/2016/07/29/the-road-from-abap-to-hcp-microservices/", true);					
		},
		
		onBlogBusFramework : function() {
			sap.m.URLHelper.redirect("https://blogs.sap.com/2015/03/24/bus-screen-framework-a-short-introduction/", true);
		},
		
		onBlogTimeoutPage : function() {
			sap.m.URLHelper.redirect("https://blogs.sap.com/2014/03/10/custom-timeout-page-for-web-dynpro-applications/", true);					
		},



		onScrollToContact : function() {
			this.scrollToSection("sectionContact");
		},
		
		/* =========================================================== */
		/* internal method */
		/* =========================================================== */
		
		/**
		 * Convenience method for setting the view model in every controller of the application
		 * 
		 * @public
		 * @param {sap.ui.model.Model} oModel the model instance
		 * @param {string} sName the model name
		 * @returns {sap.ui.mvc.View} the view instance
		 */
		setModel: function(oModel, sName) {
			return this.getView().setModel(oModel, sName);
		},
		
		/**
		 * Convenience method for getting the view model by name in every controller of the application
		 * 
		 * @public
		 * @param {string} sName the model name
		 * @returns {sap.ui.model.Model} the model instance
		 */
		getModel: function(sName) {
			return this.getView().getModel(sName);
		},
				
		/**
		 * Scroll to given section
		 * 
		 * @public
		 * @param {string} sSectionId he section id
		 */
		scrollToSection : function(sSectionId) {
			var oSection = this.getView().byId(sSectionId);			
			this.getView().byId("objectPage").scrollToSection(oSection.getId(), 450);
		}
		
	});

});